@php
    $title=$post->title;
    $description=$post->description;
    $keywords='Новости, Генератор, Суперпроект';
@endphp
@extends('layouts.app')

@section('content')
    <head>
        <h1 itemprop="headline">{{ $post->h1 }}</h1>
        <div class="row metadata hidden-xs">
            <div class="col-lg-4 col-md-4 col-sm-4">
                @foreach($post->categories as $category)
                    <a class="label label-main"
                       href="{{ route('category', ['slug' => $category->slug]) }}">{{ $category->name }}</a>
                @endforeach
                <div class="media_date">
                    {{ $post->published_at }}
                    <meta itemprop="datePublished" content="{{ $post->published_at }}" style="">
                    <meta itemprop="dateModified" content="{{ $post->published_at }}" style="">
                </div>
            </div>
        </div>
        {{--<h2 itemprop="description">Вкусные и полезные витамины, которые можно выпить</h2>--}}
        {{--    </head>--}}
        <section class="content" itemprop="articleBody">
            <hr>
            <p>{{$post->text}}</p>
        </section>
        <div class="container">
            <h2>
                Оправить комментарий
            </h2>
            <div class="row">
                <div class="col-lg-4">
                    <form action="{{ route('comment', ['slug' => $post->slug]) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="author">Имя</label>
                            <input type="text" class="form-control" id="author" name="author"
                                   placeholder="Введите ваше имя"
                                   value="{{ old('author') }}">
                            @if ($errors->has('author'))
                                <div class="invalid-feedback d-block">
                                    {{ $errors->first('author') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="comment">Комментарий</label>
                            <textarea class="form-control" id="comment" name="comment"
                                      rows="3">{{ old('comment') }}</textarea>
                            @if ($errors->has('comment'))
                                <div class="invalid-feedback d-block">
                                    {{ $errors->first('comment') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <button style="background: #28ff4c;" type="submit" class="btn btn-success">Отправить
                                комментарий
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2>Коментарии</h2>
                    <hr>
                    @foreach($post->comments as $comment)
                        <ul class="first_comments_level" id="comments_list">
                            <li>
                                <div class="text">
                                    <b><span itemprop="creator name">{{ $comment['name'] }}</span></b>
                                    <p itemprop="commentText">{{ $comment['text'] }}</p>
                                    <div class="clear"></div>
                                </div>
                            </li>
                        </ul>
                    @endforeach
                </div>
            </div>
        </div>
    </head>

@endsection

{{--    <form action="{{ route('comment', ['id' => $post->id]) }}" method="post" name="commentform" id="commentform">--}}
{{--        @csrf--}}
{{--        <p><input type="text" name="author" id="author" value="" size="25" />--}}
{{--            <small> Имя</small>--}}
{{--        @if ($errors->has('author'))--}}
{{--            <div class="invalid-feedback d-block">--}}
{{--                {{ $errors->first('author') }}--}}
{{--            </div>--}}
{{--            @endif--}}

{{--        </p>--}}
{{--        --}}{{--<p><input type="text" name="email" id="email" value="" size="25" />--}}
{{--            --}}{{--<small> Mail</small>--}}
{{--        --}}{{--</p>--}}
{{--        <p><textarea name="comment" id="comment" cols="48" rows="8"> </textarea>--}}
{{--        </p>--}}
{{--            @if ($errors->has('comment'))--}}
{{--                <div class="invalid-feedback d-block">--}}
{{--                    {{ $errors->first('comment') }}--}}
{{--                </div>--}}
{{--            @endif--}}
{{--        <p><input name="submit" type="submit" id="submit" value="Отправить комментарий" />--}}
{{--        </p>--}}
{{--    </form>--}}
