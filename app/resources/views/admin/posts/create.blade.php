@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.posts.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="h1">H1</label>
                        <input type="text" class="form-control" id="h1" name="h1" placeholder="H1"
                               value="{{ old('h1') }}">
                        @if ($errors->has('h1'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('h1') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                               value="{{ old('title') }}">
                        @if ($errors->has('title'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('title') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description"
                                  rows="3">{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('description') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text">Text</label>
                        <textarea class="form-control" id="text" name="text" rows="8">{{ old('text') }}</textarea>
                        @if ($errors->has('text'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('text') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control" value="{{ $dateNow }}" id="published_at" name="published_at"
                                   placeholder="YYYY-MM-DD HH:MM:SS" value="{{ old('published_at') }}"/>
                            @if ($errors->has('published_at'))
                                <div class="invalid-feedback d-block">
                                    {{ $errors->first('published_at') }}
                                </div>
                            @endif
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar">
                                </span>
                            </span>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker2').datetimepicker({
                                locale: 'ru'
                            });
                        });
                    </script>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="publish_now" name="publish_now">
                        <label class="form-check-label" for="publish_now">Публиковать сейчас</label>
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent category</label>
                        <select multiple="multiple" class="selectpicker form-control" id="parent_id" name="parent_id[]"
                                data-live-search="true">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('parent_id') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--<script>--}}
        {{--let publish_now = document.getElementById('publish_now');--}}
        {{--let published_at = document.getElementById('published_at');--}}
        {{--publish_now.onchange = () => {--}}
            {{--console.log(publish_now);--}}
            {{--// published_at.setAttribute("type", 'hidden');--}}
        {{--}--}}
    {{--</script>--}}
@endsection
