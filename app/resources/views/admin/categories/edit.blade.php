@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.categories.update', ["id"=>$category->id]) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                               value="{{ $category->name }}">
{{--TODO:add some if to value with old()--}}
                        @if ($errors->has('name'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="h1">H1</label>
                        <input type="text" class="form-control" id="h1" name="h1" placeholder="H1"
                               value="{{ $category->h1 }}">
                        @if ($errors->has('h1'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('h1') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                               value="{{ $category->title }}">
                        @if ($errors->has('title'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('title') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text">Slug</label>
                        <input class="form-control" id="slug" type="text" name="text" placeholder="Slug"
                               value="{{ $category->slug }}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description" rows="5">{{ $category->description }}</textarea>
                        @if ($errors->has('description'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('description') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent category</label>
                        <select class="form-control" id="parent_id" name="parent_id">
                            @if($category->parent_id == 0 )
                                <option value="0" selected="selected">
                                    Parent category
                                </option>
                            @elseif($category->parent_id !== 0 )
                                <option value="0">
                                    Parent category
                                </option>
                            @endif
                            @foreach($parentCategories as $parentCategory)
                                @if($parentCategory->id == $category->parent_id)
                                    <option value="{{ $parentCategory->id }}" selected="selected">
                                        {{ $parentCategory->name }}
                                    </option>
                                @else
                                    <option value="{{ $parentCategory->id }}">
                                        {{ $parentCategory->name }}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('parent_id') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.categories.delete', ['id'=>$category->id]) }}" method='POST'>
                    @method('DELETE')
                    @csrf
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
