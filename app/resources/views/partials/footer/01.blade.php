<footer class="footer">
    <div class="container">
        <button class="footer_menu_open hidden-lg" id="open_footer">Список разделов <span
                class="icon icon-arrow_small_down"></span>
        </button>
        <div class="footer_dropdown_menu">
            <nav class="footer_menu">
                <ul>
                    @foreach($categories as $parent)
                        <li><a href="{{ $parent->slug }}">{{ $parent->name }}</a></li>
                    @endforeach
                </ul>
            </nav>
            <nav class="footer_submenu">
                @foreach($categories as $parent)
                    @if ($parent->children->count())
                        <ul>
                            @foreach ($parent->children as $child)
                                <li><a href="{{ $child->slug }}">{{ $child->name }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                @endforeach
            </nav>
            <button class="footer_menu_hide hidden-lg" id="hide_footer">Свернуть <span
                    class="icon icon-arrow_small_up"></span>
            </button>
        </div>
        <div class="privacy">
            <p>&copy; 2019 Lara News. Все права защищены.
                <br>
                <br><a href="{{ url('/') }}">Условия перепечатки и другого использования</a> материалов
                сайта Lara News.
                <a href="{{ url('/') }}">Правила пользования</a>
                <a href="{{ url('/') }}">Политика в сфере конфиденциальности и персональных
                    данных.</a>
                <br>
                <br>Для удобства пользования сайтом используются Cookies. <a href="{{ url('/') }}">Узнать
                    больше.</a>
            </p>
        </div>
    </div>
</footer>

<a id="scroll-top" class="scroll-top"></a>
