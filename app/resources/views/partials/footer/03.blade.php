<footer class="footer">
    <div class="container">
        <button class="footer_menu_open hidden-lg" id="open_footer">Список разделов <span
                class="icon icon-arrow_small_down"></span>
        </button>
        <div class="footer_dropdown_menu">
            <nav class="footer_menu">
                <ul>
                    @foreach($categories as $parent)
                        <li><a href="{{ $parent->slug }}">{{ $parent->name }}</a></li>
                    @endforeach
                </ul>
            </nav>
            <button class="footer_menu_hide hidden-lg" id="hide_footer">Свернуть <span
                    class="icon icon-arrow_small_up"></span>
            </button>
        </div>
    </div>
</footer>

<a id="scroll-top" class="scroll-top"></a>
