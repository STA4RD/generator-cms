<div id="navbar" class="collapse navbar-collapse">

    <div class="hidden-lg sidebar-header">

        <span class="icon icon-language"></span>

        <a class="ru active">Русский</a>
        <a class="ua"
           onclick="$.cookie('lang', null); $.cookie('lang', 'ua', {expires: 100, path: '/', domain: ''});"
           href="/ua/">Українська</a>

        <button class="navbar-toggle" id="navbar_hide">
            <span class="icon icon-menu_button_close"></span>
        </button>

    </div>

    <div class="scroll_menu">
        <ul class="nav navbar-nav">
            @foreach($categories as $parent)
                <li class="dropdown menu_news ">
                    <a href="{{ '/categories/'.$parent->slug }}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">
                        <span>{{ $parent->name }}</span>
                        <span class="icon icon-arrow_small_down hidden-lg"></span>
                        <span class="icon icon-arrow_small_up hidden-lg"></span>
                    </a>
                    @if ($parent->children->count())
                        <span class="down_icon"></span>
                        <ul role="menu" class="dropdown-menu">
                            @foreach ($parent->children as $child)
                                <li><a href="{{ '/categories/'.$child->slug }}">{{ $child->name }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach

            <li class="dropdown  hidden-lg">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span>Контакты</span>
                    <span class="icon icon-arrow_small_down"></span>
                    <span class="icon icon-arrow_small_up"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="">Контакты</a></li>
                    <li><a href="">Наши спецпроекты</a></li>
                    <li><a href="">Команда</a></li>
                </ul>
            </li>
        </ul>
    </div>


    <div class="socials hidden-lg">

        <p>Читай <span class="hide_link">Lara News</span> в социальных сетях:</p>

        <div class="b_social-links">
            <a href="" target="_blank" class="fb" title="Facebook"><span
                        class="social-icon"><span class="icon icon-socials_logo_facebook"></span></span></a>
            <a href="" target="_blank" class="instagram" title="Instagram"><span
                        class="social-icon"><span class="icon icon-socials_logo_instagram"></span></span></a>
            <a href="" target="_blank" class="gplus"
               title="Google plus"><span class="social-icon"><span class="icon icon-socials_logo_gplus"></span></span></a>
            <a href="" target="_blank" class="telegram"><span class="social-icon"
                                                              title="Telegram">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                             id="Layer_1" x="0px" y="0px" viewBox="0 0 48 48" style="enable-background:new 0 0 48 48;"
                             xml:space="preserve" width="40px" height="40px">
                            <path style="fill:#FFFFFF;"
                                  d="M34.176,14.811l-3.817,19.492c0,0-0.534,1.352-2.002,0.703l-8.809-6.84l-3.203-1.568l-5.392-1.838  c0,0-0.828-0.297-0.908-0.946c-0.08-0.649,0.934-1,0.934-1l21.436-8.516C32.414,14.297,34.176,13.513,34.176,14.811z"/>
                            <path style="fill:#D2E5F1;"
                                  d="M18.87,34.083c0,0-0.257-0.024-0.578-1.052c-0.32-1.027-1.949-6.434-1.949-6.434L29.29,18.27  c0,0,0.747-0.46,0.721,0c0,0,0.133,0.081-0.267,0.46s-10.171,9.273-10.171,9.273"/>
                            <path style="fill:#B5CFE4;"
                                  d="M22.925,30.788l-3.484,3.217c0,0-0.272,0.209-0.57,0.078l0.667-5.976"/>
                         </svg>
                      </span>
            </a>
            <a href="" target="_blank" class="tw"><span class="social-icon"
                                                        title="Twitter"><span
                            class="icon icon-socials_logo_twitter"></span></span></a>
        </div>
    </div>

</div>
