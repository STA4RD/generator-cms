<nav class="navbar navbar-default navbar-fixed-top" id="toolbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" id="navbar_show">
                <span class="icon icon-menu_button"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('logo.svg') }}" alt=""><span>PNEWS</span>
            </a>
        </div>

        <div class="navbar_overlay" id="navbar_overlay"></div>

        @if($settings['navbar'] === 1)
            @include('partials.navbar.01')
        @elseif($settings['navbar'] === 2)
            @include('partials.navbar.02')
        @elseif($settings['navbar'] === 3)
            @include('partials.navbar.03')
        @endif

        <div class="search visible-lg" id="search_box">
            <div class="container_search">
                <input type="text" id="search_input" placeholder="поисковый запрос" name="q">
            </div>
            <button class="search_btn" id="search_btn">
                <span class="icon icon-search_button"></span>
            </button>
        </div>
    </div>
</nav>

@if($settings['promobar'] === 1)
    @include('partials.promobar.01')
@elseif($settings['promobar'] === 2)
    @include('partials.promobar.02')
@elseif($settings['promobar'] === 3)
    @include('partials.promobar.03')
@endif
