<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|max:255',
            'title'       => 'required|max:255',
            'slug'        => 'sometimes|max:255',
            'description' => 'required|max:255',
            'h1'          => 'required|max:255',
            'parent_id'   => 'required|integer'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'        => 'Название обязательна для заполнение',
            'slug.sometimes'       => 'Slug обязательна для заполнение',
            'title.required'       => 'Заголовок обязательна для заполнение',
            'description.required' => 'Описание обязательна для заполнение',
            'h1.required'          => 'H1 обязательна для заполнение',
            'parent_id.required'   => 'Главная категория обязательно',
            'parent_id.integer'    => 'Parent_Id должно быть числом',
        ];
    }
}
