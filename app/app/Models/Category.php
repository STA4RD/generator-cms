<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'title', 'slug', 'description', 'h1', 'parent_id',
    ];

    public function children() {
        return $this->hasMany(static::class, 'parent_id')->orderBy('name', 'asc');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }
}
