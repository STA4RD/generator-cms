<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $howMach = 8000;
        foreach (range(1, $howMach) as $index) {
            DB::table('comments')->insert([
                'name' => $faker->name,
                'text' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                'post_id' => $faker->numberBetween($min = 1, $max = 2000),
            ]);
        }
    }
}
